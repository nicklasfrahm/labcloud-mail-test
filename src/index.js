const nodemailer = require("nodemailer");
const { config } = require("dotenv");

config();

// Create closure as usage of async / await is not allowed in global scope.
(async () => {
  // Create reusable transporter object by use of the default SMTP transport.
  console.log("Creating transport via SMTP.");
  const transporter = nodemailer.createTransport({
    host: process.env.SMTP_SERVER,
    port: 465,
    secure: true,
    auth: {
      user: process.env.SMTP_USER,
      pass: process.env.SMTP_PASSWORD
    }
  });

  // Send mail with defined transport object.
  console.log("Sending mail via SMTP transport.");
  await transporter.sendMail({
    from: '"SDU - The Core" <thecore@sdu.dk>',
    to: "nfrah16@student.sdu.dk",
    subject: "Test",
    text: "Does this work?",
    html: "<b>Does this work?</b>"
  });
})().catch(console.error);
