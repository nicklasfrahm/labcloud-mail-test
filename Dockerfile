# base image
FROM node:10-alpine

# working directory
WORKDIR /usr/src/app

# copy dependency lists
COPY package.json package-lock.json ./

# install dependencies
RUN npm install --production

# copy source files
COPY . ./

# configure static build variables
ARG TAG

# configure dynamic environment variables
ENV SMTP_SERVER=""
ENV SMTP_USER=""
ENV SMTP_PASSWORD=""
ENV TAG=${TAG}

# start application
CMD [ "node", "src/index" ]
